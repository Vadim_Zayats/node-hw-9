import { Strategy } from "passport-http-bearer";
import { LoginError } from "../services/errorHandler";
import { DecodedToken } from "../interfaces/interfaces";
import jwt from "jsonwebtoken";
import { Users } from "../db/entity/User";
import { AppDataSource } from "../db/data-source";

const userRepository = AppDataSource.getRepository(Users);

const authMiddleware = async (
  token: string,
  done: (err: Error | null, user?: any) => void
) => {
  const decodedData: DecodedToken = await new Promise((resolve, reject) =>
    jwt.verify(token, "secret", async (err, decoded: any) => {
      if (err) {
        reject(null);
      } else {
        resolve(decoded);
      }
    })
  );
  const { email, password } = decodedData;
  let err: any = null;
  const isUserExist = await userRepository.findOneBy({
    email,
  });

  if (err) {
    console.log("authMiddleware ERROR", err);
    // throw new LoginError("Користувач не авторизований"); // ?????
    return done(err);
  }
  if (!isUserExist) {
    console.log("authMiddleware NO USER");
    // throw new LoginError("Користувач не авторизований"); // ?????
    return done(null, null);
  }

  if (decodedData.password !== password) {
    console.log("authMiddleware WRONG PASSWORD");
    //      throw new LoginError("Користувач не авторизований"); // ?????
    return done(null, null);
  }
  console.log("authMiddleware SUCCESS", decodedData);
  done(null, decodedData);
};

export const bearerStrategy = new Strategy(authMiddleware);
