import * as fsPromise from "fs/promises";
import { Random } from "random-js";
import fs from "fs";
import {
  Data,
  Schema,
  InputData,
  UpdatedData,
  GetTableOperations,
} from "../interfaces/interfaces";
import { AppDataSource } from "../db/data-source";
import { Users } from "../db/entity/User";
import "reflect-metadata";

export class FileDB {
  private schemas: { [key: string]: Schema } = {};
  private static instance: FileDB;

  private constructor() {}

  static getInstance(): FileDB {
    if (!FileDB.instance) {
      FileDB.instance = new FileDB();
    }
    return FileDB.instance;
  }

  async registerSchema(tableName: string, schema: Schema): Promise<void> {
    this.schemas[tableName] = schema;
    // const filePath = `fake-db/${tableName}.json`;

    // // Перевірка наявності папки
    // if (!fs.existsSync("fake-db")) {
    //   try {
    //     fs.mkdirSync("fake-db");
    //     console.log(`Папка "fake-db" створена успішно.`);
    //   } catch (err) {
    //     console.error(`Помилка при створенні папки "fake-db": `, err);
    //   }
    // }
    // try {
    //   await fsPromise.writeFile(filePath, JSON.stringify([], null, 2));
    //   console.log(`Створено файл бази даних ${tableName}`);
    // } catch (error) {
    //   console.error(
    //     `Помилка при створенні файлу бази даних ${tableName}:`,
    //     error
    //   );
    // }
  }

  async saveData(tableName: string, data: Data[]): Promise<void> {
    // const filePath = `fake-db/${tableName}.json`;
    // await this.saveDataToFile(filePath, data);
  }

  async readData(tableName: string): Promise<any | undefined> {
    // const filePath = `fake-db/${tableName}.json`;
    // const gottedData: Data[] | undefined = await this.readDataFromFile(
    //   filePath
    // );
    // const timedData = gottedData?.map(({ createDate, ...other }) => {
    //   return { ...other, createDate: new Date(createDate) };
    // });
    // return timedData;
  }

  getTable(tableName: string): GetTableOperations {
    return new GetTableOperationsInstance(tableName);
  }
}

class GetTableOperationsInstance implements GetTableOperations {
  private tableName: string;

  constructor(tableName: string) {
    this.tableName = tableName;
  }

  async getAll(
    page: number,
    size: number
  ): Promise<{ paginatedData: Data[]; totalLength: number } | undefined> {
    try {
      const allData = await fileDB.readData(this.tableName);
      if (allData !== undefined) {
        const startIndex = (page - 1) * size;
        const paginatedData = allData.slice(startIndex, startIndex + size);
        return { paginatedData, totalLength: allData.length };
      } else {
        return undefined;
      }
    } catch (error) {
      console.error("Error fetching all data:", error);
      return undefined;
    }
  }

  async getById(id: number): Promise<Data | undefined> {
    const allData = await fileDB.readData(this.tableName);
    if (allData !== undefined) {
      return allData.find((item: Data) => item.id === id);
    } else {
      return undefined;
    }
  }

  async create(data: InputData): Promise<Data> {
    let db = await fileDB.readData(this.tableName);
    if (!db) {
      db = [];
    }

    const newData: Data = {
      id: new Random().integer(10000, 99999),
      ...data,
      createDate: new Date(),
    };

    db.push(newData);

    await fileDB.saveData(this.tableName, db);
    return newData;
  }

  async update(
    id: number,
    newData: Partial<UpdatedData>
  ): Promise<Data | undefined> {
    let allData: Data[] | undefined = await fileDB.readData(this.tableName);
    if (allData !== undefined) {
      const targetData = allData.find((item: Data) => item.id === id);
      if (targetData) {
        const updatedData = {
          ...targetData,
          ...newData,
        };
        // Оновлюємо елемент у масиві
        allData = allData.map((item: Data) =>
          item.id === id ? updatedData : item
        );
        await fileDB.saveData(this.tableName, allData);
        return updatedData;
      }
    }
  }

  async delete(id: number): Promise<number | undefined | null> {
    let allData = await fileDB.readData(this.tableName);
    if (allData !== undefined) {
      const indexToDelete = allData.findIndex(
        (item: Data | undefined) => item && item.id === id
      );
      if (indexToDelete !== -1) {
        const deletedId = allData[indexToDelete].id;
        allData = allData.filter(
          (item: Data | undefined) => item && item.id !== id
        );
        await fileDB.saveData(this.tableName, allData);
        console.log(`Пост з id "${id}" видалено`);
        return deletedId;
      }
      console.log(`Посту з id "${id}" не існує`);
      return null;
    }
  }
}

export const fileDB = FileDB.getInstance();

export const newspostTable = fileDB.getTable("newsposts");
