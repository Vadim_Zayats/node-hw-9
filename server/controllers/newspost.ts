import { Request, Response } from "express";
import { newspostTable } from "../services/crud-db-service";
import { checkPostService } from "../validation/posts";
import {
  ValidationError,
  NewspostsServiceError,
} from "../services/errorHandler";

class NewsPostController {
  async getAllPosts(req: Request, res: Response) {
    const page = parseInt(req.query.page as string) || 1;
    const size = parseInt(req.query.size as string) || 10;

    const allPosts = await newspostTable.getAll(page, size);
    if (!allPosts) {
      throw new NewspostsServiceError(
        `Помилка на сервері при отриманні усіх постів`
      );
    }

    return res.status(200).json(allPosts);
  }

  async getPostById(req: Request, res: Response) {
    const post = await newspostTable.getById(parseInt(req.params.id));
    if (!post) {
      throw new NewspostsServiceError(
        `Помилка на сервері при отриманні посту по id: ${parseInt(
          req.params.id
        )}`
      );
    }
    return res.status(200).json(post);
  }

  async createNewPost(req: Request, res: Response) {
    const check: any = checkPostService(req.body);
    if (check?.length > 0) {
      throw new ValidationError(check[0].message);
    }
    const newPost = await newspostTable.create(req.body);
    return res.status(200).json(newPost);
  }

  async editPost(req: Request, res: Response) {
    // валідація змінених полів
    const check: any = checkPostService(req.body);
    if (check?.length > 0) {
      throw new ValidationError(check[0].message);
    }
    // оновлення посту
    const updatedPost = await newspostTable.update(
      parseInt(req.params.id),
      req.body
    );
    if (!updatedPost) {
      throw new NewspostsServiceError("Пост не знайдено");
    }
    return res.status(200).json(updatedPost);
  }
  async deletePost(req: Request, res: Response) {
    // видалення
    const updatedPost = await newspostTable.delete(parseInt(req.params.id));
    if (!updatedPost) {
      throw new NewspostsServiceError("Пост не знайдено");
    }
    return res.status(200).json(updatedPost);
  }
}

export default new NewsPostController();
